<?php
$I = new WebGuy($scenario);
$I->wantTo('log in as regular user');
$I->amOnPage('/login.php');
$I->fillField('username','admin');
$I->fillField('password','password123');
$I->click('Login');
$I->see('Hello, admin');
?>